import './styles/index.scss'

const SumElements = (arr) => {
    let sum = 0
    for (let element of arr) {
        sum += element
    }
    console.log(sum) // 220
  }
  SumElements([10, 20, 40, 60, 90])